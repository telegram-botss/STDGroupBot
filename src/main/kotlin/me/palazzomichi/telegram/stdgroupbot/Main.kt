package me.palazzomichi.telegram.stdgroupbot

import io.github.ageofwar.telejam.Bot
import me.palazzomichi.telegram.summarybot.*
import me.palazzomichi.telegram.titlechangerbot.tryLoadTitles
import java.nio.file.NoSuchFileException
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.TimeUnit
import io.github.ageofwar.manymexbot.tryLoadConfig as tryLoadManyMexConfig
import me.palazzomichi.telegram.summarybot.tryLoadStrings as tryLoadSummaryStrings
import me.palazzomichi.telegram.titlechangerbot.tryLoadStrings as tryLoadTitleChangerStrings

private val BOT_PROPERTIES_PATH = Paths.get(".", "bot.properties")

fun main() = try {
    val botProperties = loadBotProperties(BOT_PROPERTIES_PATH)
    val (token, chatId, titlesPath, titleChangerStringsPath,
            titleChangeDelay, regexesPath, summaryStringsPath,
            maxSummarySize, manyMexConfigPath) = botProperties
    val bot = Bot.fromToken(token)
    val chat = bot.getChat(chatId)
    val titles = tryLoadTitles(titlesPath)
    val titleChangerStrings = tryLoadTitleChangerStrings(titleChangerStringsPath)
    val topics = TreeSet<Topic> { t1, t2 ->
        when {
            t1.message.date > t2.message.date -> 1
            t1.message.date < t2.message.date -> -1
            else -> t1.message.text.hashtags.indexOf(t1.name) -
                    t2.message.text.hashtags.indexOf(t2.name)
        }
    }.withMaxSize(maxSummarySize)
    val regexes = tryLoadRegexes(regexesPath).toMutableSet()
    val summaryStrings = tryLoadSummaryStrings(summaryStringsPath)
    val manyMexConfig = tryLoadManyMexConfig(manyMexConfigPath)
    StdGroupBot(bot, chat,
            titles, titleChangeDelay, titlesPath, titleChangerStrings,
            topics, regexes, regexesPath, summaryStrings,
            manyMexConfig).use {
        it.run()
    }
} catch (e: NoSuchFileException) {
    BotProperties(
            token = "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11",
            chatId = -123456789L,
            titlesPath = Paths.get(".", "TitleChanger", "titles.txt"),
            titleChangerStringsPath = Paths.get(".", "TitleChanger", "strings.json"),
            titleChangeDelay = TimeUnit.DAYS.toMillis(1),
            regexsPath = Paths.get(".", "Summary", "regexes.txt"),
            summaryStringsPath = Paths.get(".", "Summary", "strings.json"),
            maxSummarySize = 50,
            manyMexConfigPath = Paths.get(".", "ManyMex", "config.json")
    ).save(BOT_PROPERTIES_PATH)
}
