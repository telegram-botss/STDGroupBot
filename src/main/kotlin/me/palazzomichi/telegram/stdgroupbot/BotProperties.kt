package me.palazzomichi.telegram.stdgroupbot

import java.nio.file.Files
import java.nio.file.Files.createDirectories
import java.nio.file.Files.newOutputStream
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption.CREATE
import java.nio.file.StandardOpenOption.TRUNCATE_EXISTING
import java.util.*

data class BotProperties(
        val token: String,
        val chatId: Long,
        val titlesPath: Path,
        val titleChangerStringsPath: Path,
        val titleChangeDelay: Long,
        val regexsPath: Path,
        val summaryStringsPath: Path,
        val maxSummarySize: Int,
        val manyMexConfigPath: Path
)

fun loadBotProperties(path: Path): BotProperties {
    val botProperties = Properties().apply {
        load(Files.newInputStream(path))
    }
    val token = botProperties.getNotNullProperty("token")
    val chatId = botProperties.getNotNullProperty("chat-id").toLong()
    val titlesPath = botProperties.getNotNullProperty("titles-path")
    val titleChangerStringsPath = botProperties.getNotNullProperty("title-changer-strings-path")
    val titleChangeDelay = botProperties.getNotNullProperty("title-change-delay").toLong()
    val regexsPath = botProperties.getNotNullProperty("regexs-path")
    val summaryStringsPath = botProperties.getNotNullProperty("summary-strings-path")
    val maxSummarySize = botProperties.getNotNullProperty("max-summary-size").toInt()
    val manyMexConfigPath = botProperties.getNotNullProperty("many-mex-config-path")
    return BotProperties(
            token, chatId,
            Paths.get(titlesPath), Paths.get(titleChangerStringsPath), titleChangeDelay,
            Paths.get(regexsPath), Paths.get(summaryStringsPath), maxSummarySize,
            Paths.get(manyMexConfigPath)
    )
}

private fun Properties.getNotNullProperty(property: String) = getProperty(property)
        ?: throw MissingPropertyException(property)

fun BotProperties.save(path: Path) {
    createDirectories(path.parent)
    val botProperties = Properties().apply {
        setProperty("token", token)
        setProperty("chat-id", chatId.toString())
        setProperty("titles-path", titlesPath.toString())
        setProperty("title-changer-strings-path", titleChangerStringsPath.toString())
        setProperty("title-change-delay", titleChangeDelay.toString())
        setProperty("regexs-path", regexsPath.toString())
        setProperty("summary-strings-path", summaryStringsPath.toString())
        setProperty("max-summary-size", maxSummarySize.toString())
        setProperty("many-mex-config-path", manyMexConfigPath.toString())
    }
    botProperties.store(newOutputStream(path, CREATE, TRUNCATE_EXISTING), null)
}

class MissingPropertyException(property: String) : Exception("cannot find property '$property'")
