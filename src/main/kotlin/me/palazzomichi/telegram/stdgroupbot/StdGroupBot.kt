package me.palazzomichi.telegram.stdgroupbot

import io.github.ageofwar.manymexbot.Config
import io.github.ageofwar.manymexbot.ManyMexBot
import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.LongPollingBot
import io.github.ageofwar.telejam.chats.Chat
import io.github.ageofwar.telejam.updates.Update
import me.palazzomichi.telegram.summarybot.MutableRegexes
import me.palazzomichi.telegram.summarybot.SummaryBot
import me.palazzomichi.telegram.summarybot.Topic
import me.palazzomichi.telegram.titlechangerbot.TitleChangerBot
import me.palazzomichi.telegram.titlechangerbot.Titles
import java.nio.file.Path
import me.palazzomichi.telegram.summarybot.Strings as SummaryStrings
import me.palazzomichi.telegram.titlechangerbot.Strings as TitleChangerStrings

class StdGroupBot(
        bot: Bot,
        chat: Chat,
        titles: Titles,
        titleChangeDelay: Long,
        titlesPath: Path,
        titleChangerStrings: TitleChangerStrings,
        topics: MutableSet<Topic>,
        regexes: MutableRegexes,
        regexesPath: Path,
        summaryStrings: SummaryStrings,
        manyMexConfig: Config
) : LongPollingBot(bot) {
    private val summaryBot = SummaryBot(bot, chat, topics, regexes, regexesPath, summaryStrings)
    private val titleChangerBot = TitleChangerBot(bot, chat, titles, titleChangeDelay, titlesPath, titleChangerStrings)
    private val manyMexBot = ManyMexBot(bot, manyMexConfig)

    override fun onUpdate(update: Update) {
        titleChangerBot.onUpdate(update)
        summaryBot.onUpdate(update)
        manyMexBot.onUpdate(update)
        super.onUpdate(update)
    }
}
